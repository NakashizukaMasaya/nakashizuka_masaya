<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div class="main-contents">
            <div class="header">
            	<div class="title">ユーザー管理画面</div>
            	<div class="menu">
			        <a href="./">ホーム</a>
			        <a href="signup">ユーザー新規登録</a>
			    </div>
            </div>

			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

            <div class="users">
			    <c:forEach items="${users}" var="user">
			        <div class="user">
		                <div class="account">アカウント：<c:out value="${user.account}" /></div>
		                <div class="name">名前：<c:out value="${user.name}" /></div>
		                <div class="branchName">支社：<c:out value="${user.branchName}" /></div>
		                <div class="departmentName">部署：<c:out value="${user.departmentName}" /></div>
						<div class="isStopped">稼働状態：<c:out value="${user.isStoppedName}" /></div>
			            <form action="setting" method="get">
			            	<input type="hidden" name="id" value="${user.id}">
			            	<input type="submit" value="編集">
		            	</form>
		            	<c:if test="${user.id != loginUser.id}">
			            	<form action="stop" method="post">
				            	<input type="hidden" name="id" value="${user.id}">
			            		<c:if test="${user.isStopped == 0 }">
			            			<input type="hidden" name="changeIsStoppedTo" value="1">
			            			<input type="submit" value="停止" onclick="return confirm('本当に停止させますか？')">
			            		</c:if>
			            		<c:if test="${user.isStopped == 1 }">
			            			<input type="hidden" name="changeIsStoppedTo" value="0">
			            			<input type="submit" value="復活" onclick="return confirm('本当に復活させますか？')">
			            		</c:if>
			            	</form>
			            </c:if>
			        </div>
			    </c:forEach>
			</div>

            <div class="copyright"> Copyright&copy;nakashizuka_masaya</div>
        </div>
    </body>
</html>