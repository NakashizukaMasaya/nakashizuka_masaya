<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ホーム画面</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div class="main-contents">
            <div class="header">
            	<div class="title">ホーム画面</div>
            	<div class="menu">
			        <a href="message">新規投稿</a>
			        <c:if test="${loginUser.departmentId == 1 }">
			        	<a href="management">ユーザー管理</a>
			        </c:if>
			        <a href="logout">ログアウト</a>
			    </div>
            </div>

			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="narrow-down form-area">
				<form action="./" method="get">
					<div class="date-select">
			            <label for="startDate">日付</label>
			            <input type="date" name="startDate" id="startDate" value="${startDate}"/>～
			            <label for="endDate"></label>
			            <input type="date" name="endDate" id="endDate" value="${endDate}"/>
					</div>
					<div class ="category-select">
						<label for="category">カテゴリ</label>
			            <input name="category" id="category" value="${category}"/>
					</div>
					<input type="submit" value="絞込み">
				</form>
			</div>

            <div class="messages">
			    <c:forEach items="${messages}" var="message">
			    	<div class="message-and-comment">
				        <div class="message">
				        	<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			                <div class="user-name"><c:out value="${message.userName}" /></div>
			                <div class="title"><c:out value="${message.title}" /></div>
			                <div class="category">カテゴリ：<c:out value="${message.category}" /></div>
				            <div class="text"><pre><c:out value="${message.text}"/></pre></div>

							<c:if test="${message.userId == loginUser.id}">
					            <form action="deleteMessage" method="post">
					            	<input type="hidden" name="messageId" value="${message.messageId}">
					            	<input type="submit" value="削除" onclick="return confirm('本当に削除しますか？')">
				            	</form>
							</c:if>
				        </div>
						<div class="comments">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${comment.messageId == message.messageId }">
									<div class="comment">
					            		<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
										<div class="user-name"><c:out value="${comment.userName}" />からのコメント</div>
				                		<div class="text"><pre><c:out value="${comment.text}" /></pre></div>

										<c:if test="${comment.userId == loginUser.id}">
											<form action="deleteComment" method="post">
								            	<input type="hidden" name="commentId" value="${comment.commentId}">
								            	<input type="submit" value="削除" onclick="return confirm('本当に削除しますか？')">
							            	</form>
							            </c:if>
									</div>
								</c:if>
							</c:forEach>
						</div>
			            <div class="form-area">
		       				<form action="comment" method="post">
		       					<input type="hidden" name="messageId" value="${message.messageId}">
					            <textarea name="text" id="text" cols="30" rows="1"></textarea>
					            <input type="submit" value="コメントする">
				            </form>
			            </div>
			        </div>
			    </c:forEach>
			</div>

            <div class="copyright"> Copyright&copy;nakashizuka_masaya</div>
        </div>
    </body>
</html>