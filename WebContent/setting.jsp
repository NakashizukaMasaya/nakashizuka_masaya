<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー編集画面</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        	<div class="header">
        		<div class="title">ユーザー編集画面</div>
            	<div class="menu"><a href="management">ユーザー管理</a></div>
            </div>

            <c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
	            <form action="setting" method="post"><br />
	            	<input type="hidden" name="id" value="${user.id}">

	                <label for="account">アカウント</label>
	                <input name="account" id="account" value="${user.account}"/> <br />

	                <label for="password">新しいパスワード</label>
	                <input name="password" type="password" id="password"/> <br />

	                <label for="confirmPassword">確認用新しいパスワード</label>
	                <input name="confirmPassword" type="password" id="confirmPassword"/> <br />

	                <label for="name">名前</label>
	                <input name="name" id="name" value="${user.name}"/><br />

					<c:if test="${(user.id == loginUser.id) && (user.departmentId == 1)}">
						<p>本社・総務人事部の方は、ご自身の支社・部署を変更できません</p>
						<input type="hidden" name="branchId" value="${user.branchId}">
						<input type="hidden" name="departmentId" value="${user.departmentId}">
					</c:if>
					<c:if test="${!((user.id == loginUser.id) && (user.departmentId == 1))}">
		                <label for="branch">支社</label>
		                <select name="branchId" id="branch" >
		                	<c:forEach items="${branches}" var="branch">
		                		<c:choose>
		                			<c:when test="${user.branchId == branch.id}">
		                				<option selected value="${branch.id}"><c:out value="${branch.name}"></c:out></option>
		               				</c:when>
		               				<c:otherwise>
		               					<option value="${branch.id}"><c:out value="${branch.name}"></c:out></option>
		               				</c:otherwise>
		               			</c:choose>
		               		</c:forEach>
		                </select> <br />

		                <label for="department">部署</label>
		                <select name="departmentId" id="department" >
		                	<c:forEach items="${departments}" var="department">
		                		<c:choose>
		                			<c:when test="${user.departmentId == department.id}">
		                				<option selected value="${department.id}"><c:out value="${department.name}"></c:out></option>
		               				</c:when>
		               				<c:otherwise>
		               					<option value="${department.id}"><c:out value="${department.name}"></c:out></option>
		               				</c:otherwise>
		               			</c:choose>
		                	</c:forEach>
		                </select> <br />
					</c:if>
	                <input type="submit" value="更新" /> <br />

	            </form>
	        </div>

            <div class="copyright">Copyright&copy;nakashizuka_masaya</div>
        </div>
    </body>
</html>