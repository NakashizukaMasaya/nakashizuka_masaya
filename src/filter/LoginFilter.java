package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	HttpServletRequest hrequest = (HttpServletRequest) request;
    	String path = hrequest.getServletPath();

    	//ログインページ以外にアクセスしようとした場合、以下を実行
        if (!(path.equals("/login")) && (!(path.equals("/css/style.css")))) {
        	HttpSession session = hrequest.getSession();

        	//ログインユーザーがいないなら、以下を実行
        	if(session.getAttribute("loginUser") == null) {
	        	HttpServletResponse hresponse = (HttpServletResponse) response;
	        	List<String> errorMessages = new ArrayList<String>();
	        	errorMessages.add("ログインしてください");
	        	session.setAttribute("errorMessages", errorMessages);

	        	hresponse.sendRedirect("login");
	        	return;
        	}
        }

        // ログイン画面にアクセスしようとしているなら、ログインサーブレットを実行
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void destroy() {
    }

}