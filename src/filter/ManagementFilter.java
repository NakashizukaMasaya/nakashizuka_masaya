package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/management", "/signup", "/setting"})
public class ManagementFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	HttpServletRequest hrequest = (HttpServletRequest) request;
    	HttpSession session = hrequest.getSession();
    	User loginUser = (User) session.getAttribute("loginUser");

    	//ログインユーザーの所属が本社の総務人事部でないなら、以下を実行
    	if((loginUser.getBranchId() != 1) && (loginUser.getDepartmentId() != 1)){
        	HttpServletResponse hresponse = (HttpServletResponse) response;
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("権限のないユーザーです");
        	session.setAttribute("errorMessages", errorMessages);

        	hresponse.sendRedirect("./");
        	return;
    	}

        // ログインユーザーの部署が総務人事部なら、サーブレットを実行
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void destroy() {
    }

}