package beans;

import java.io.Serializable;

public class UserBranchDepartment implements Serializable {

    private int id;
    private String account;
    private String name;
    private String branchName;
    private String departmentName;
    private int isStopped;
    private String isStoppedName;

    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public String getAccount() {
    	return account;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getName() {
    	return name;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public String getBranchName() {
    	return branchName;
    }

    public void setBranchName(String branchName) {
    	this.branchName = branchName;
    }

    public String getDepartmentName() {
    	return departmentName;
    }

    public void setDepartmentName(String departmentName) {
    	this.departmentName = departmentName;
    }

    public int getIsStopped() {
    	return isStopped;
    }

    public void setIsStopped(int isStopped) {
    	this.isStopped = isStopped;
    	setIsStoppedName(isStopped);
    }

    public String getIsStoppedName() {
    	return isStoppedName;
    }

    //setIsStoppedメソッドからしか呼び出さない
    private void setIsStoppedName(int isStopped) {
    	if(isStopped == 0) {
    		this.isStoppedName = "稼働中";
    	}else {
    		this.isStoppedName = "停止中";
    	}

    }
}