package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {

	private int commentId;
	private int messageId;
    private String text;
    private int userId;
    private String userName;
    private Date createdDate;

    public int getCommentId() {
    	return commentId;
    }

    public void setCommentId(int commentId) {
    	this.commentId = commentId;
    }

    public int getMessageId() {
    	return messageId;
    }

    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }

    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }

    public int getUserId() {
    	return userId;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public String getUserName() {
    	return userName;
    }

    public void setUserName(String userName) {
    	this.userName = userName;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
}