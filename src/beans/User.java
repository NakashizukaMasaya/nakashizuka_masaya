package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private int id;
    private String account;
    private String password;
    private String confirmPassword;
    private String name;
    private int branchId;
    private int departmentId;
    private int isStopped;
    private String isStoppedName;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public String getAccount() {
    	return account;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getPassword() {
    	return password;
    }

    public void setPassword(String password) {
    	this.password = password;
    }

    public String getConfirmPassword() {
    	return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
    	this.confirmPassword = confirmPassword;
    }

    public String getName() {
    	return name;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public int getBranchId() {
    	return branchId;
    }

    public void setBranchId(int branchId) {
    	this.branchId = branchId;
    }

    public int getDepartmentId() {
    	return departmentId;
    }

    public void setDepartmentId(int departmentId) {
    	this.departmentId = departmentId;
    }

    public int getIsStopped() {
    	return isStopped;
    }

    public void setIsStopped(int isStopped) {
    	this.isStopped = isStopped;
    	setIsStoppedName(isStopped);
    }

    public String getIsStoppedName() {
    	return isStoppedName;
    }

    //setIsStoppedメソッドからしか呼び出さない
    private void setIsStoppedName(int isStopped) {
    	if(isStopped == 0) {
    		this.isStoppedName = "稼働中";
    	}else {
    		this.isStoppedName = "停止中";
    	}

    }

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
    	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }
}