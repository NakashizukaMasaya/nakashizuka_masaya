package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	List<String> errorMessages = new ArrayList<String>();

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        User user = new UserService().select(account, password);

        if(!(isValid(account, password, user, errorMessages))) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("inputAccount", account);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
        	return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    //バリデーション
    private boolean isValid(String account, String password, User user, List<String> errorMessages) {

    	if(StringUtils.isBlank(account)) {
        	errorMessages.add("アカウント名を入力してください");
    	}

    	if(StringUtils.isBlank(password)) {
        	errorMessages.add("パスワードを入力してください");
    	}

    	//アカウントもパスワードも入力しているが、組み合わせがおかしいorアカウントが停止状態
    	if((errorMessages.size() == 0) && ((user == null) || (user.getIsStopped() == 1))) {
        	errorMessages.add("アカウント名とパスワードの組み合わせが不正です");
    	}

    	if(errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}