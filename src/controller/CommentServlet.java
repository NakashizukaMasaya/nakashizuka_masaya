package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Comment comment = getComment(request, session);

        if(!isValid(comment, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        //コメントをDBに登録
        new CommentService().insert(comment);

        response.sendRedirect("./");
    }

    //リクエストパラメータをComment型に整形
    private Comment getComment(HttpServletRequest request, HttpSession session) throws IOException, ServletException {
    	Comment comment = new Comment();
        comment.setText(request.getParameter("text"));
        comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
        User user = (User) session.getAttribute("loginUser");
        comment.setUserId(user.getId());
        return comment;
    }

    //コメントバリデーション
    private boolean isValid(Comment comment, List<String> errorMessages) {
        String text = comment.getText();

        if(StringUtils.isBlank(text)) {
            errorMessages.add("コメント本文を入力してください");
        }else if(500 < text.length()){
        	errorMessages.add("コメントは500文字以下で入力してください");
        }

        if(errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}