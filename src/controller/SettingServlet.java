package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();
		String id = request.getParameter("id");

    	//URLに数字以外の値か、存在しないユーザーIDを入力してアクセスされたら、編集画面に戻す
    	if((!(id.matches("[0-9]+"))) || (new UserService().select(Integer.parseInt(id)) == null)){
    		errorMessages.add("不正なパラメータが入力されました");
    		session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("management");
            return;
    	}

    	User user = new UserService().select(Integer.parseInt(id));

    	List<Branch> branches = new BranchService().select();
  	  	List<Department> departments = new DepartmentService().select();
  	  	request.setAttribute("user", user);
  	  	request.setAttribute("branches", branches);
	  	request.setAttribute("departments", departments);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	List<String> errorMessages = new ArrayList<String>();
        User user = getUser(request);

        if(!isValid(user, errorMessages)) {
        	List<Branch> branches = new BranchService().select();
      	  	List<Department> departments = new DepartmentService().select();
      	  	request.setAttribute("user", user);
      	  	request.setAttribute("branches", branches);
    	  	request.setAttribute("departments", departments);
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

        new UserService().update(user);

        response.sendRedirect("management");
    }

    //リクエストパラメータをUser型に整形
    private User getUser(HttpServletRequest request) throws IOException, ServletException {
        User user = new User();
    	user.setId(Integer.parseInt(request.getParameter("id")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirmPassword(request.getParameter("confirmPassword"));
        user.setName(request.getParameter("name"));
    	user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
    	user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

        return user;
    }

    //ユーザー登録バリデーション
    private boolean isValid(User user, List<String> errorMessages) {
    	int id = user.getId();
    	String account = user.getAccount();
        String password = user.getPassword();
        String confirmPassword = user.getConfirmPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        User someone = new UserService().select(account);//入力したアカウント名と合致するアカウント名を持つユーザー

        if(StringUtils.isBlank(account)) {
            errorMessages.add("アカウント名を入力してください");
        }else if(!(account.matches("[a-zA-Z0-9]{6,20}"))) {
        	errorMessages.add("アカウント名は6文字以上20文字以下の半角英数字で入力してください");
        }else if((someone != null) && (someone.getId() != id)) {
        	errorMessages.add("そのアカウント名は使用できません");
        }

        if(!(StringUtils.isBlank(password))) {
	        if(!(password.matches("[ -~]{6,20}"))){
	        	errorMessages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
	        }else if(!(password.equals(confirmPassword))) {
	        	errorMessages.add("入力されたパスワードと確認用パスワードが合致していません");
	        }
        }

        if(StringUtils.isBlank(name)) {
        	errorMessages.add("名前を入力してください");
        }else if(10 < name.length()) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        if(((branchId == 1) && (2 < departmentId)) || ((1 < branchId) && (departmentId < 3))) {
        	errorMessages.add("支社と部署の組み合わせが不正です");
        }

        if(errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}