package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	//全ての支社情報を取得
    public List<Branch> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    id, ");
            sql.append("    name, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append("FROM branches ");
            sql.append("ORDER BY id ASC");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<Branch> branches = toBranchs(rs);
            return branches;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //セレクト結果をリストに整形
    private List<Branch> toBranchs(ResultSet rs) throws SQLException {

        List<Branch> branches = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                Branch branch = new Branch();
                branch.setId(rs.getInt("id"));
                branch.setName(rs.getString("name"));
                branch.setCreatedDate(rs.getTimestamp("created_date"));
                branch.setUpdatedDate(rs.getTimestamp("updated_date"));

                branches.add(branch);
            }
            return branches;
        } finally {
            close(rs);
        }
    }
}