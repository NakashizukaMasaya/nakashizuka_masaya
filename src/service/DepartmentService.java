package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService {

    //全ての部署情報を取得
    public List<Department> select() {
        Connection connection = null;
        try {
            connection = getConnection();

            List<Department> departments = new DepartmentDao().select(connection);
            commit(connection);

            return departments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}