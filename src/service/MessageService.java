package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	//新規投稿
    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
            return;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ホームに投稿を表示
    public List<UserMessage> select(String startDate, String endDate, String category) {
        Connection connection = null;
        try {
            connection = getConnection();

            if(StringUtils.isBlank(startDate)) {
            	startDate = "2020-01-01*00*00*00";
            }else {
            	startDate = startDate + "*00*00*00";
            }
            if(StringUtils.isBlank(endDate)) {
            	endDate = new SimpleDateFormat("yyyy-MM-dd*hh*mm*ss").format(new Date());
            }else {
            	endDate = endDate + "*23*59*59";

            }

            List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, category);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
		} finally {
            close(connection);
        }
    }

    //投稿を削除
    public void delete(int messageId) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}